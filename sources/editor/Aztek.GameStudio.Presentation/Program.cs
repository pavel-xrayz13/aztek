﻿using Aztek.Core.DI;
using Aztek.Core.Framework;
using System;
using System.Windows.Forms;

namespace Aztek.GameStudio.Presentation
{
    static class Program
    {
        [MTAThread]
        static async void Main()
        {
            const string modulesDirectory = "modules";

            IContainer container = new Container();
            var moduleManager = new ModuleManager(container, modulesDirectory);

            // Start all modules
            await moduleManager.Initialize();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrameView());
        }
    }
}
