﻿using System;
using System.Collections.Generic;

namespace Aztek.Core.BaseTypes.Extensions
{
    public static class ForEachExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            IEnumerator<T> enumerator = collection.GetEnumerator();
            while (enumerator.MoveNext())
            {
                action.Invoke(enumerator.Current);
            }
        }
    }
}
