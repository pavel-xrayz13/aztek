﻿using System;

namespace Aztek.Core.BaseTypes.Extensions
{
    public static class EnumFlagsExtensions
    {
        public static TEnum SetFlag<TEnum>(this Enum input, TEnum @enum) where TEnum : Enum
        {
            if (!input.GetType().IsEquivalentTo(typeof(TEnum)))
            {
                throw new ArgumentException("Enum value and flags types don't match.");
            }

            // yes this is ugly, but unfortunately we need to use an intermediate boxing cast
            var val = (TEnum)Enum.ToObject(typeof(TEnum), Convert.ToUInt64(input) | Convert.ToUInt64(@enum));
            input = val;

            return val;
        }

        public static TEnum ClearFlag<TEnum>(this Enum input, TEnum @enum) where TEnum : Enum
        {
            if (!input.GetType().IsEquivalentTo(typeof(TEnum)))
            {
                throw new ArgumentException("Enum value and flags types don't match.");
            }

            // yes this is ugly, but unfortunately we need to use an intermediate boxing cast
            var val = (TEnum)Enum.ToObject(typeof(TEnum), Convert.ToUInt64(input) & ~Convert.ToUInt64(@enum));
            input = val;

            return val;
        }
    }
}
