﻿using System;
using System.Collections.Generic;

namespace Aztek.Core.BaseTypes.Exceptions
{
    /// <summary>
    /// Used in ParallelForEachAsync&lt;T&gt; extension method
    /// </summary>
    [Serializable]
    public class ParallelForEachException : AggregateException
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ParallelForEachException(IEnumerable<Exception> innerExceptions)
            : base(innerExceptions)
        {
        }
    }
}
