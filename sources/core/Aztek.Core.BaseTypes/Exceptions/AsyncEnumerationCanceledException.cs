﻿using System;

namespace Aztek.Core.BaseTypes.Exceptions
{
    /// <summary>
    /// This exception is thrown when you call <see cref="AsyncEnumerator{T}.Yield.Break"/>
    /// or when the enumerator is disposed before reaching the end of enumeration.
    /// </summary>
    [Serializable]
    public sealed class AsyncEnumerationCanceledException : OperationCanceledException { }
}
