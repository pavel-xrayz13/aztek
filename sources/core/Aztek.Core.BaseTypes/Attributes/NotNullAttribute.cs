﻿using System;

namespace Aztek.Core.BaseTypes.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Parameter | 
        AttributeTargets.Property | AttributeTargets.Delegate | AttributeTargets.Field)]
    public class NotNullAttribute : Attribute
    {
    }
}
