﻿using System;

namespace Aztek.Core.BaseTypes.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Parameter |
        AttributeTargets.Property | AttributeTargets.Delegate | AttributeTargets.Field)]
    public class DescriptionAttribute : Attribute
    {
        public DescriptionAttribute(string desc)
        {
            GetDescription = desc;
        }

        public string GetDescription { get; }
    }
}
