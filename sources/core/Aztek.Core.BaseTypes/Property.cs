﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.BaseTypes.Constants;
using System;
using System.Diagnostics.Contracts;
using System.Numerics;

namespace Aztek.Core.BaseTypes
{
    /// <summary cref="Property">
    ///   Class for providing a method to transfer paramters between a system and the framework.
    /// </summary>
    public class Property
    {
        private object value;
        private Type type;

        /// <summary cref="Property(string, Enum)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">String value to be placed inside.</param>
        public Property([NotNull] string name, [NotNull] Enum value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            Enumeration = value;
            AccessFlags = flags;
        }

        /// <summary cref="Property(string, int)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">String value to be placed inside.</param>
        public Property([NotNull] string name, int value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            Int32 = value;
            AccessFlags = flags;
        }

        /// <summary cref="Property(string, uint)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">String value to be placed inside.</param>
        public Property([NotNull] string name, uint value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            Uint32 = value;
            AccessFlags = flags;
        }

        /// <summary cref="Property(string, string)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">String value to be placed inside.</param>
        public Property([NotNull] string name, [NotNull] string value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            String = value;
            AccessFlags = flags;
        }

        /// <summary cref="Property(string, Vector2)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">3D-vector value to be placed inside.</param>
        public Property([NotNull] string name, Vector2 value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            Vector2D = value;
            AccessFlags = flags;
        }

        /// <summary cref="Property(string, Vector3)">
        ///   Constructor for creating a well formed property.  Useful for creating an array of
        ///   statically defined properties.
        /// </summary>
        /// <param name="name">The name of this property.</param>
        /// <param name="value">3D-vector value to be placed inside.</param>
        public Property([NotNull] string name, Vector3 value, PropertyAccessFlags flags = PropertyAccessFlags.Default)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            Name = name;
            Vector3D = value;
            AccessFlags = flags;
        }

        public string Name { get; }

        public PropertyAccessFlags AccessFlags { get; set; }

        public bool IsEnumeration => type == typeof(Enum);

        public bool IsInt32 => type == typeof(int);

        public bool IsUint32 => type == typeof(uint);

        public bool IsFloat => type == typeof(float);

        public bool IsString => type == typeof(string);

        public bool IsVector2D => type == typeof(Vector2);

        public bool IsVector3D => type == typeof(Vector3);

        public bool IsVector4D => type == typeof(Vector4);

        public bool IsQuaternion => type == typeof(Quaternion);

        public Enum Enumeration
        {
            get
            {
                Contract.Requires(IsEnumeration);
                if (value == null)
                    return default;

                return (Enum)value;
            }

            set
            {
                type = typeof(Enum);
                this.value = value;
            }
        }

        public int Int32
        {
            get
            {
                Contract.Requires(IsInt32);
                if (value == null)
                    return default;

                return (int)value;
            }

            set
            {
                type = typeof(int);
                this.value = value;
            }
        }

        public uint Uint32
        {
            get
            {
                Contract.Requires(IsUint32);
                if (value == null)
                    return default;

                return (uint)value;
            }

            set
            {
                type = typeof(uint);
                this.value = value;
            }
        }

        public float Float
        {
            get
            {
                Contract.Requires(IsFloat);
                if (value == null)
                    return default;

                return (float)value;
            }

            set
            {
                type = typeof(float);
                this.value = value;
            }
        }

        public string String
        {
            get
            {
                Contract.Requires(IsString);
                return value as string;
            }

            set
            {
                Contract.Requires(!string.IsNullOrEmpty(value));
                type = typeof(string);
                this.value = value;
            }
        }

        public Vector2 Vector2D
        {
            get
            {
                Contract.Requires(IsVector2D);
                if (value == null)
                    return default;

                return (Vector2)value;
            }

            set
            {
                type = typeof(Vector2);
                this.value = value;
            }
        }

        public Vector3 Vector3D
        {
            get
            {
                Contract.Requires(IsVector3D);
                if (value == null)
                    return default;

                return (Vector3)value;
            }

            set
            {
                type = typeof(Vector3);
                this.value = value;
            }
        }

        public Vector4 Vector4D
        {
            get
            {
                Contract.Requires(IsVector4D);
                if (value == null)
                    return default;

                return (Vector4)value;
            }

            set
            {
                type = typeof(Vector4);
                this.value = value;
            }
        }

        public Quaternion Quat
        {
            get
            {
                Contract.Requires(IsQuaternion);
                if (value == null)
                    return default;

                return (Quaternion)value;
            }

            set
            {
                type = typeof(Quaternion);
                this.value = value;
            }
        }
    }
}
