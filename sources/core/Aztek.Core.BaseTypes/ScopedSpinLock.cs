﻿using System;
using System.Threading;

namespace Aztek.Core.BaseTypes
{
    public class ScopedSpinLock : IDisposable
    {
        private bool isDisposed = false;
        private readonly bool useMemoryBarrierOnExit;
        private readonly SpinLock spinLock;

        public ScopedSpinLock(SpinLock spinLock, TimeSpan span, bool useMemoryBarrierOnExit = false)
        {
            var lockTaken = false;
            this.useMemoryBarrierOnExit = useMemoryBarrierOnExit;
            this.spinLock = spinLock;

            while (!lockTaken)
            {
                spinLock.TryEnter(ref lockTaken);
                if (!lockTaken)
                {
                    Thread.Sleep(span);
                }
            }
        }

        public void Dispose()
        {
            if(!isDisposed)
            {
                isDisposed = true;
            }

            if(!isDisposed)
            {
                DisposeInternal();
            }
        }

        private void DisposeInternal()
        {
            spinLock.Exit(useMemoryBarrierOnExit);
        }
    }
}
