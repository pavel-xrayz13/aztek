﻿namespace Aztek.Core.BaseTypes.Constants
{
    /// <summary cref="GlobalSceneStatus">
    ///   Enumeration for informing the <c>SystemScene</c> on what is occuring.
    ///   <para>
    ///     Pre and post loading are used to inform when the framework being and ends
    ///     completely loading a scene.
    ///   </para>
    ///   <para>
    ///     Pre and post destroying are used to inform when the framework being and ends
    ///     completely destroying a scene.
    ///   </para>
    /// </summary>
    public enum GlobalSceneStatus
    {
        PreLoadingObjects,
        PostLoadingObjects,
        PreDestroyingObjects,
        PostDestroyingObjects
    }
}
