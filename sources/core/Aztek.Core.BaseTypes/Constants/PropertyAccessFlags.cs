﻿using System;

namespace Aztek.Core.BaseTypes.Constants
{
    [Flags]
    public enum PropertyAccessFlags
    {
        Default = 0,
        Valid = 0x1,
        InitOnly = 0x2,
        Multiple = 0x4, // this is write only by default
        WriteOnly = 0x8
    }
}
