﻿using System.Threading.Tasks;

namespace Aztek.Core.BaseTypes.Async.Generics.Internals
{
    internal static class TaskEx
    {
        public static readonly Task<bool> True = Task.FromResult(true);
        public static readonly Task<bool> False = Task.FromResult(false);
        public static readonly Task Completed = True;
    }
}