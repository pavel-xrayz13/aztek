﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.BaseTypes.Extensions;
using Aztek.Core.Interface;
using Aztek.Core.Framework.Modules;
using Aztek.Core.Framework.Modules.Impl;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Aztek.Core.DI;
using System.Diagnostics.Contracts;

namespace Aztek.Core.Framework
{
    public class ModuleManager
    {
        private readonly IContainer container;
        private readonly ICollection<IModule> modules;
        private readonly IModuleScaner moduleScaner;

        public ModuleManager([NotNull] IContainer container, [NotNull] string path)
        {
            Contract.Requires<ArgumentNullException>(container != null, nameof(container));
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(path), nameof(path));

            this.container = container;
            moduleScaner = new ModuleScaner(new ModuleInitializer(container), path);
        }

        public async Task Initialize()
        {
            var modules = moduleScaner.DetectModules();
            await modules.ForEachAsync(async c => await c.RegisterModuleAsync(container));
        }
    }
}
