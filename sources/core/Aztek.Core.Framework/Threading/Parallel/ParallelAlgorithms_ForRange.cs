﻿using System;
using System.Collections.Concurrent;
using STT = System.Threading.Tasks;

namespace Aztek.Core.Framework.Threading.Parallel
{
    public static partial class ParallelAlgorithms
    {
        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            int fromInclusive, int toExclusive, 
            Action<int, int> body)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions, body);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            int fromInclusive, int toExclusive,
            Action<int, int, STT.ParallelLoopState> body)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions, body);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="localInit">The function delegate that returns the initial state of the local data for each thread.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <param name="localFinally">The delegate that performs a final action on the local state of each thread.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange<TLocal>(
            int fromInclusive, int toExclusive,
            Func<TLocal> localInit,
            Func<int, int, STT.ParallelLoopState, TLocal, TLocal> body,
            Action<TLocal> localFinally)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions,
                localInit, body, localFinally);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            long fromInclusive, long toExclusive,
            Action<long, long> body)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions, body);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            long fromInclusive, long toExclusive,
            Action<long, long, STT.ParallelLoopState> body)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions, body);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="localInit">The function delegate that returns the initial state of the local data for each thread.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <param name="localFinally">The delegate that performs a final action on the local state of each thread.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange<TLocal>(
            long fromInclusive, long toExclusive,
            Func<TLocal> localInit,
            Func<long, long, STT.ParallelLoopState, TLocal, TLocal> body,
            Action<TLocal> localFinally)
        {
            return ForRange(fromInclusive, toExclusive, s_defaultParallelOptions,
                localInit, body, localFinally);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="parallelOptions">A ParallelOptions instance that configures the behavior of this operation.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            int fromInclusive, int toExclusive,
            STT.ParallelOptions parallelOptions,
            Action<int, int> body)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (body == null)
                throw new ArgumentNullException("body");

            return STT.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, range =>
            {
                body(range.Item1, range.Item2);
            });
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="parallelOptions">A ParallelOptions instance that configures the behavior of this operation.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            int fromInclusive, int toExclusive,
            STT.ParallelOptions parallelOptions,
            Action<int, int, STT.ParallelLoopState> body)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (body == null)
                throw new ArgumentNullException("body");

            return STT.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, (range, loopState) =>
            {
                body(range.Item1, range.Item2, loopState);
            });
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="localInit">The function delegate that returns the initial state of the local data for each thread.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <param name="localFinally">The delegate that performs a final action on the local state of each thread.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange<TLocal>(
            int fromInclusive, int toExclusive,
            STT.ParallelOptions parallelOptions,
            Func<TLocal> localInit,
            Func<int, int, STT.ParallelLoopState, TLocal, TLocal> body,
            Action<TLocal> localFinally)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (localInit == null)
                throw new ArgumentNullException("localInit");

            if (body == null)
                throw new ArgumentNullException("body");

            if (localFinally == null)
                throw new ArgumentNullException("localFinally");

            return STT.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, localInit, (range, loopState, x) =>
            {
                return body(range.Item1, range.Item2, loopState, x);
            }, localFinally);
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="parallelOptions">A ParallelOptions instance that configures the behavior of this operation.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            long fromInclusive, long toExclusive,
            STT.ParallelOptions parallelOptions,
            Action<long, long> body)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (body == null)
                throw new ArgumentNullException("body");

            return System.Threading.Tasks.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, range =>
            {
                body(range.Item1, range.Item2);
            });
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="parallelOptions">A ParallelOptions instance that configures the behavior of this operation.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange(
            long fromInclusive, long toExclusive,
            STT.ParallelOptions parallelOptions,
            Action<long, long, STT.ParallelLoopState> body)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (body == null)
                throw new ArgumentNullException("body");

            return STT.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, (range, loopState) =>
            {
                body(range.Item1, range.Item2, loopState);
            });
        }

        /// <summary>Executes a for loop over ranges in which iterations may run in parallel. </summary>
        /// <param name="fromInclusive">The start index, inclusive.</param>
        /// <param name="toExclusive">The end index, exclusive.</param>
        /// <param name="localInit">The function delegate that returns the initial state of the local data for each thread.</param>
        /// <param name="body">The delegate that is invoked once per range.</param>
        /// <param name="localFinally">The delegate that performs a final action on the local state of each thread.</param>
        /// <returns>A ParallelLoopResult structure that contains information on what portion of the loop completed.</returns>
        public static STT.ParallelLoopResult ForRange<TLocal>(
            long fromInclusive, long toExclusive,
            STT.ParallelOptions parallelOptions,
            Func<TLocal> localInit,
            Func<long, long, STT.ParallelLoopState, TLocal, TLocal> body,
            Action<TLocal> localFinally)
        {
            if (parallelOptions == null)
                throw new ArgumentNullException("parallelOptions");

            if (localInit == null)
                throw new ArgumentNullException("localInit");

            if (body == null)
                throw new ArgumentNullException("body");

            if (localFinally == null)
                throw new ArgumentNullException("localFinally");

            return STT.Parallel.ForEach(Partitioner.Create(fromInclusive, toExclusive), parallelOptions, localInit, (range, loopState, x) =>
            {
                return body(range.Item1, range.Item2, loopState, x);
            }, localFinally);
        }
    }
}