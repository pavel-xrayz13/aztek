﻿using System.Threading.Tasks;

namespace Aztek.Core.Framework.Threading.Parallel
{
    /// <summary>
    /// Provides parallelized algorithms for common operations.
    /// </summary>
    public static partial class ParallelAlgorithms
    {
        // Default, shared instance of the ParallelOptions class.  This should not be modified.
        private static ParallelOptions s_defaultParallelOptions = new ParallelOptions();
    }
}
