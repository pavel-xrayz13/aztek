﻿using Aztek.Core.BaseTypes.Async.Generics;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface;
using System.Reflection;

namespace Aztek.Core.Framework.Modules
{
    internal interface IModuleInitializer
    {
        IAsyncEnumerable<IModule> CreateInstance([NotNull] Assembly assembly);
    }
}
