﻿using Aztek.Core.BaseTypes.Async.Generics;
using Aztek.Core.Interface;

namespace Aztek.Core.Framework.Modules
{
    internal interface IModuleScaner
    {
        IAsyncEnumerable<IModule> DetectModules();
    }
}
