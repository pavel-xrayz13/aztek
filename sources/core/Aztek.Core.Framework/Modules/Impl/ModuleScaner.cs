﻿using Aztek.Core.BaseTypes.Async.Generics;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.BaseTypes.Extensions;
using Aztek.Core.Interface;
using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Aztek.Core.Framework.Modules.Impl
{
    internal class ModuleScaner : IModuleScaner
    {
        private readonly IModuleInitializer initializer;
        private readonly string path;

        public ModuleScaner([NotNull] IModuleInitializer initializer, [NotNull] string path)
        {
            Contract.Requires<ArgumentNullException>(initializer != null, nameof(initializer));
            Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(path), nameof(path));
            this.initializer = initializer;
            this.path = path;
        }

        public IAsyncEnumerable<IModule> DetectModules()
        {
            return new AsyncEnumerable<IModule>(async yield =>
            {
                var modules = GetAllModuleFromPath();
                await modules.ParallelForEachAsync(async asm =>
                {
                    await initializer.CreateInstance(asm).ForEachAsync(async module =>
                    {
                        await yield.ReturnAsync(module);
                    });
                });
            });
        }

        private IAsyncEnumerable<Assembly> GetAllModuleFromPath()
        {
            var directoryInfo = new DirectoryInfo(path);
            return GetAllModulesFromDirectory(directoryInfo);
        }

        private IAsyncEnumerable<Assembly> GetAllModulesFromDirectory(DirectoryInfo dir)
        {
            return new AsyncEnumerable<Assembly>(async c =>
            {
                foreach (var entry in dir.GetFiles())
                {
                    var assembly = await FilterAssemblyForIModule(entry);
                    if (assembly == null)
                        continue;

                    await c.ReturnAsync(assembly);
                }
            });
        }

        private Task<Assembly> FilterAssemblyForIModule([NotNull] FileInfo fileInfo)
        {
            return Task.Factory.StartNew(() =>
            {
                var assembly = Assembly.LoadFrom(fileInfo.FullName);
                bool hasIModules = assembly.GetTypes().Any(t => t.IsAssignableFrom(typeof(IModule)));
                if (!hasIModules)
                    return null;

                return assembly;
            });
        }
    }
}
