﻿using Aztek.Core.BaseTypes.Async.Generics;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.DI;
using Aztek.Core.Interface;
using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;

namespace Aztek.Core.Framework.Modules.Impl
{
    internal class ModuleInitializer : IModuleInitializer
    {
        private readonly IContainer container;

        public ModuleInitializer([NotNull] IContainer container)
        {
            Contract.Requires<ArgumentNullException>(container != null, nameof(container));
            this.container = container;
        }

        public IAsyncEnumerable<IModule> CreateInstance([NotNull] Assembly assembly)
        {
            return new AsyncEnumerable<IModule>(async yield =>
            {
                var types = assembly.GetTypes().Select(t => t).Where(t => t.IsAssignableFrom(typeof(IModule))).ToArray();
                foreach(var type in types)
                {
                    var activatedType = Activator.CreateInstance(type) as IModule;
                    await yield.ReturnAsync(activatedType);
                }
            });
        }
    }
}
