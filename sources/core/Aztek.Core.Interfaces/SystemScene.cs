﻿using Aztek.Core.BaseTypes;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.BaseTypes.Constants;
using Aztek.Core.Interface.Constants;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Aztek.Core.Interface
{
    /// <summary>
    ///   <c>SystemScene</c> is an abstract class for managing a scene or scenes in a system.
    /// </summary>
    public abstract class SystemScene : ChangeControlSubject, IChangeControlObserver
    {
        protected bool isInitialized = false;

        /// <summary cref="SystemScene(SystemComponent)">
        ///   Constructor.
        /// </summary>
        /// <param name="systemComponent">The system component this scene belongs to.</param>
        protected SystemScene([NotNull] SystemComponent systemComponent)
        {
            Contract.Requires(systemComponent != null);
            System = systemComponent;
        }

        /// <summary cref="Initialize(IEnumerable{Property})">
        ///   One time initialization function for the object.
        /// </summary>
        /// <param name="Properties">Property structure array to fill in.</param>
        /// <returns>An error code.</returns>
        public abstract ResultCodes Initialize([NotNull] IEnumerable<Property> properties);

        /// <summary cref="CreateObject(string, string)">
        ///   Creates a system object used to extend a UObject.
        /// </summary>
        /// <param name="name">The unique name for this object.</param>
        /// <param name="type">The object type to create.</param>
        /// <returns>The newly created system object.</returns>
        [NotNull] public abstract SystemObject CreateObject([NotNull] string name, [NotNull] string type);

        /// <summary cref="DestroyObject(SystemObject)">
        ///   Destroys a system object.
        /// </summary>
        /// <param name="pSystemObject">The system object to destroy.</param>
        /// <returns>An error code.</returns>
        public abstract ResultCodes DestroyObject([NotNull] SystemObject systemObject);

        /// <summary cref="Properties">
        ///   Properties of this object.
        /// </summary>
        public abstract IEnumerable<Property> Properties
        {
            /// <summary>
            ///   Gets the properties of this object.
            /// </summary>
            /// <returns>The Property structure array to fill.</returns>
            [NotNull] get;

            /// <summary>
            ///   Sets the properties for this object.
            /// </summary>
            [NotNull] set;
        }

        /// <summary cref="GlobalSceneStatusChanged(GlobalSceneStatus)">
        ///   Called from the framework to inform the scene extension of the overall scene status.
        /// </summary>
        /// <param name="Status">The overall scene status.</param>
        public virtual void GlobalSceneStatusChanged(GlobalSceneStatus status)
        {
        }

        /// <summary cref="ChangeOccurred(IChangeControlSubject, ChangeBitMask)">
        ///   Implementation of the <c>IChangeControlObserver</c> ChangeOccurred method.
        /// </summary>
        /// <seealso cref="IChangeControlObserver.ChangeOccurred(IChangeControlSubject, ChangeBitMask)"/>
        public virtual ResultCodes ChangeOccurred([NotNull] IChangeControlSubject subject, ChangeBitMask changeBitMask)
        {
            return ResultCodes.Success;
        }

        /// <summary cref="DesiredSystemChanged">
        ///   Returns a bit mask of System Changes that this scene wants to receive changes for. Used
        ///   to inform the change control manager if this scene should be informed of the
        ///   change.
        /// </summary>
        /// <returns>A change bit mask of system scene.</returns>
        public virtual ChangeBitMask DesiredSystemChanged => ChangeBitMask.None;

        /// <summary cref="System">
        ///   Gets the system component this object belongs to.
        /// </summary>
        /// <returns>A reference to the system component.</returns>
        public SystemComponent System { get; }
    }
}
