﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface.Constants;

namespace Aztek.Core.Interface
{
    /// <summary cref="IChangeControlObserver">
    ///   The <c>IObserver</c> interface supplies loosely coupling systems with dependency.  It
    ///   facilitates a lazy behaviour where by systems only need to react to a change and facilitates
    ///   dependent callback threading.
    /// <para>
    ///   This interface follows the pattern commonly know as the Observer pattern,
    ///   the Publish/Subscribe pattern, or the Dependents pattern.
    /// </para>
    /// <seealso cref="IChangeControlSubject"/>
    /// <seealso cref="IChangeControlManager"/>
    /// </summary>
    /// <remarks>
    ///   The Observer pattern is documented in "Design Patterns," written by Erich Gamma et. al.,
    ///   published by Addison-Wesley in 1995.
    /// </remarks>
    public interface IChangeControlObserver
    {
        /// <summary cref="ChangeOccurred">
        ///   Lets the ISubject notify the IObserver in changes in registered aspects of interest.
        /// </summary>
        /// <remarks> 
        ///   This method is typically called from IChangeControlManager.DistributeQueuedChanges() or 
        ///	  IChangeControlSubject.PostChanges() depending on whether the observer registered with an IChangeManager 
        ///   or an ISubject respectively.	
        /// </remarks>
        /// <param name="subject">
        ///   A pointer to the <c>IChangeControlSubject</c> interface of the component that changed.
        /// </param>
        /// <param name="changeBitMask">
        ///   The aspects of interest that changed as defined by the supplied 
        ///   IChangeControlSubject's published interest bits. if uInChangeBits are 0, 
        ///   then the subject is shutting down, and should be released. 
        ///	</param>
        ///	<seealso cref="IChangeControlSubject.PostChanges(ChangeBitMask)"/>
        /// <seealso cref="IChangeControlManager.DistributeQueuedChanges"/>
        /// <returns> One of the following Error codes:
        ///     ResultCodes.Success
        ///			No error.
        ///     ResultCodes.InvalidAddress 
        ///			pInSubject was null pointer.
        ///     ResultCodes.OutOfMemory                                   
        ///			Not enough memory is available to resolve the change.
        /// </returns>
        ResultCodes ChangeOccurred([NotNull] IChangeControlSubject subject, ChangeBitMask changeBitMask);
    }
}
