﻿using System.Numerics;

namespace Aztek.Core.Interface.Services
{
    /// <summary cref="ITargetServiceAccessor">
    ///   <c>ITargetServiceAccessor</c> is an interface for providing target related functionality.  Any
    ///    objects that modify or provide target data are required to implement this class.
    /// </summary>
    public interface ITargetServiceAccessor
    {
        /// <summary cref="GetTarget">
        ///   Return the point of interest for this object.
        /// </summary>
        Vector3 GetTarget();
    }
}
