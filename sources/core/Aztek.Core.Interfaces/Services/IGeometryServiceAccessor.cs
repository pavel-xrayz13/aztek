﻿using System.Numerics;

namespace Aztek.Core.Interface.Services
{
    /// <summary>
    ///   <c>IGeometryServiceAccessor</c> is an interface for providing geometry related functionality.  Any
    ///   objects that modify geometry data are required to implement this class.
    /// </summary>
    public interface IGeometryServiceAccessor
    {
        /// <summary>
        /// Gets the position of the object.
        /// </summary>
        Vector3 Position { get; }

        /// <summary>
        /// Gets the orientation of the object.
        /// </summary>
        Quaternion Orientation { get; }

        /// <summary>
        /// Gets the scale of the object.
        /// </summary>
        Vector3 Scale { get; }
    }
}
