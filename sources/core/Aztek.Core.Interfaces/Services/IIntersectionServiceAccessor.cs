﻿using Aztek.Core.Interface.Contracts;
using System.Collections.Generic;

namespace Aztek.Core.Interface.Services
{
    /// <summary>
    ///   <c>IIntersectionServiceAccessor</c> is an interface for providing intersection related functionality.
    ///   Any objects that modify contact data are required to implement this class.
    /// </summary>
    public interface IIntersectionServiceAccessor
    {
        /// <summary>
        ///   Returns the Intersection information.
        /// </summary>
        /// <returns>An constant array of the Intersection information.</returns>
        IReadOnlyCollection<IntersectionObjectInfo> GetIntersections();
    }
}
