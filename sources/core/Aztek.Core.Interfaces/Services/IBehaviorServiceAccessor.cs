﻿namespace Aztek.Core.Interface.Services
{
    /// <summary>
    /// <c>IBehaviorObject</c> is an interface for providing contact related functionality.  Any
    /// objects that modify contact data are required to implement this class.  FTW!
    /// </summary>
    public interface IBehaviorServiceAccessor
    {
        /// <summary>
        /// Gets the current behavior of this object.
        /// </summary>
        /// <returns>Behavior of this object.</returns>
        uint GetBehaviorType();
    }
}
