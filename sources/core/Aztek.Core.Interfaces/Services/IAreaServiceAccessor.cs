﻿namespace Aztek.Core.Interface.Services
{
    /// <summary>
    ///   <c>IAreaServiceAccessor</c> is an interface for providing Area related functionality. Any
    ///   objects that modify or provide Area data are required to implement this class.
    /// </summary>
    public interface IAreaServiceAccessor
    {
        /// <summary cref="GetAreaName">
        ///   Returns the name to id this area.
        /// </summary>
        string GetAreaName();

        /// <summary cref="IsAreaActive">
        ///   Return True if this ares is active (i.e. the area has been triggered).
        /// </summary>
        bool IsAreaActive();
    }
}
