﻿using Aztek.Core.Interface.Contracts;

namespace Aztek.Core.Interface.Services
{
    /// <summary>
    ///   <c>IContactServiceAccessor</c> is an interface for providing contact related functionality.
    ///   Any objects that modify contact data are required to implement this class.
    /// </summary>
    public interface IContactServiceAccessor
    {
        /// <summary>
        ///   Returns the object-to-object contact information.
        /// </summary>
        /// <returns>Data describing the last contact for this object.</returns>
        ContactObjectInfo GetContact();
    }
}
