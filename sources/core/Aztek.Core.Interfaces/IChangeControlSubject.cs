﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface.Constants;

namespace Aztek.Core.Interface
{
    /// <summary>
    /// The <c>ISubject</c> interface supplies loosely coupling systems with dependency
    /// It facilitates a lazy behaviour where by systems only need to react to a change
    ///	and facilitates dependent callback threading.      
    /// </summary>
    /// <seealso cref="IChangeControlObserver"/>
    /// <seealso cref="IChangeControlManager"/>
    /// <remarks>
    /// This interface follows the pattern commonly know as the Observer
    ///	pattern, the Publish/Subscribe pattern, or the Dependents pattern. 
    /// </remarks>
    /// <remarks>
    /// The Observer pattern is documented in "Design Patterns," written by
    ///	Erich Gamma et. al., published by Addison-Wesley in 1995.          
    /// </remarks>
    public interface IChangeControlSubject
    {
        /// <summary cref="PreDestruct">
        ///  Forces the Destruction 
        /// </summary>
        /// <returns>
        ///  One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///</returns>
        void PreDestruct();

        /// <summary cref="Attach">
        ///  Associates the provided <c>IChangeControlObserver</c> with the given 
        ///  <c>IChangeControlSubject</c> aspects of interest.
        /// </summary>
        /// <remarks>
        ///  This method is typically called from @e ChangeControlManager.Register()
        ///  or the IChangeControlObserver, if used without a ChangeControlManager.             
        ///  </remarks>
        /// <param name="observer"> 
        ///   A reference to the <c>IChangeControlObserver</c>.
        /// </param>
        /// <param name="mask">
        ///   The aspects of interest that changed as defined by the supplied
        ///	  ISubject's published interest bits.
        ///	</param>
        /// <param name="uID"> ID assigned by pInObserver to this subject.</param>
        /// <param name="shiftBits">
        ///   Used for components supporting multiply inherited interfaces each
        ///	  with subject interfaces.
        ///	</param>
        /// <returns>One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///     Error::OutOfMemory                                   
        ///			Not enough memory is available to resolve the change.
        ///</returns>
        ResultCodes Attach([NotNull] IChangeControlObserver observer, ChangeBitMask mask);

        /// <summary cref="Detach">
        ///  Disassociates the provided Observer with the Subject
        /// </summary>
        /// <remarks>
        ///  This method is typically called from @e ChangeManager::Register()
        ///  or the IObserver, if used without a ChangeManager.             
        ///  </remarks>
        /// <param name="observer"> 
        ///  A reference to the <c>IChangeControlObserver</c>.
        /// </param>
        /// <returns>One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///</returns>
        ResultCodes Detach([NotNull] IChangeControlObserver observer);

        /// <summary cref="UpdateInterestBits(IChangeControlObserver, ChangeBitMask)">
        /// </summary>
        /// <param name="observer"></param>
        /// <param name="bits"></param>
        /// <returns></returns>
        ResultCodes UpdateInterestBits([NotNull] IChangeControlObserver observer, ChangeBitMask bits);

        /// <summary>
        ///  Publishes to attached Observers and ChanageManager that changes have occurred.
        /// </summary>
        /// <remarks>
        ///  This method is typically called from @e ChangeManager::Register()
        ///  or the IObserver, if used without a ChangeManager.             
        ///  </remarks>
        /// <param name="changeBitMask"> 
        /// The bit field that describes the conceptual change with 
        ///	respect to the published interests.
        /// </param>
        void PostChanges(ChangeBitMask changeBitMask);

        /// <summary>
        /// Identifies the system changes that this subject could possibly make.
        /// </summary>
        /// <returns>A bitmask of the possible system changes.</returns>
        ChangeBitMask PotentialSystemChanges { get; }
    }
}
