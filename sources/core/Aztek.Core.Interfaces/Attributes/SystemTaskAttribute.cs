﻿using System;

namespace Aztek.Core.Interface.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    class SystemTaskAttribute : Attribute
    {
    }
}
