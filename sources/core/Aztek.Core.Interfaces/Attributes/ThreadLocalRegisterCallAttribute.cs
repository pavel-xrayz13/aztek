﻿using System;

namespace Aztek.Core.Interface.Attributes
{
    /// <summary>
    ///   Triggers a synchronized callback to be called once by each thread used by the task manager.
    ///   The method which uses this attribute should only be called during initialization of the task manager.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    class ThreadLocalRegisterCallAttribute : Attribute
    {
    }
}
