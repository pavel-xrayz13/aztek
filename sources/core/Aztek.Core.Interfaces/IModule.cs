﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.DI;
using System.Threading.Tasks;

namespace Aztek.Core.Interface
{
    public interface IModule
    {
        Task RegisterModuleAsync([NotNull] IContainer container);
    }
}
