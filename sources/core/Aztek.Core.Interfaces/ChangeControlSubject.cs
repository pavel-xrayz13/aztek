﻿using Aztek.Core.BaseTypes;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.BaseTypes.Extensions;
using Aztek.Core.Interface.Constants;
using Aztek.Core.Interface.Contracts.Internal;
using Aztek.Core.Interface.Extensions.Internal;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;

namespace Aztek.Core.Interface
{
    public abstract class ChangeControlSubject : IChangeControlSubject
    {
        // IMPLEMENTATION NOTE
        // Since only Change Control Managers (CCM) are supposed to subscribe for 
        // notifications sent by PostChange, there are not many observers expected 
        // to be in the list. Another assumption is that repeated attaches are infrequent.
        // Thus the most frequent operation is the traversal and thus the usual LinkedList
        // will suit fine here. See constructor for details.
        private readonly ICollection<ChangeControlSubjectRequest> observerList;
        private readonly SpinLock spinLock;

        protected ChangeControlSubject()
        {
            observerList = new LinkedList<ChangeControlSubjectRequest>();
            spinLock = new SpinLock();
        }

        public ResultCodes Attach([NotNull] IChangeControlObserver observer, ChangeBitMask mask)
        {
            // Since the intended usage model is to use this method from CCMs only, and 
            // their implementation provided by this framework ensures that pObs in nonzero
            // the following assertion should suffice.
            Contract.Assert(observer != null);

            // Framework's CCM implementation ensures that the following assertion always holds
            Contract.Requires(observerList.FirstOrDefault(m => m.Observer == observer) == null);

            // Add the observer to our list of observers
            observerList.Add(new ChangeControlSubjectRequest(observer, mask));

            return ResultCodes.Success;
        }

        public ResultCodes Detach([NotNull] IChangeControlObserver observer)
        {
            var obj = observerList.FirstOrDefault(m => m.Observer == observer);
            if (observerList.Remove(obj))
            {
                return ResultCodes.Success;
            }

            return ResultCodes.Error;
        }

        public void PostChanges(ChangeBitMask changeBitMask)
        {
            if(!observerList.Any())
            {
                return;
            }

            ICollection<KeyValuePair<IChangeControlObserver, ChangeBitMask>> postData = null;

            // Double check to avoid unnecessary lock acquisition
            using (new ScopedSpinLock(spinLock, new TimeSpan(10)))
            {
                // TODO: optimize me
                postData = new KeyValuePair<IChangeControlObserver, ChangeBitMask>[observerList.Count];

                observerList.ForEach(m =>
                {
                    var bitMask = m.GetBitsToPost(changeBitMask);
                    if (bitMask != 0)
                    {
                        var post = new KeyValuePair<IChangeControlObserver, ChangeBitMask>(m.Observer, bitMask);
                        postData.Add(post);
                    }
                });
            }

            // Posting is done outside of the lock
            postData.ForEach(m => m.Key.ChangeOccurred(this, m.Value));
        }

        public void PreDestruct()
        {
            // THREAD SAFETY NOTE
            // Currently this method is called from the destructor only (that is it is 
            // never called concurrently). Thus it does not lock the list. If ever in 
            // the future it is called concurrently, then locking similar to that
            // in the ChangeControlSubject.Detach method will have to be added.

            foreach(var observed in observerList)
            {
                observed.Observer.ChangeOccurred(this, 0);
            }
        }

        public ResultCodes UpdateInterestBits([NotNull] IChangeControlObserver observer, ChangeBitMask bits)
        {
            var obj = observerList.FirstOrDefault(m => m.Observer == observer);
            if(obj == null)
            {
                return ResultCodes.Error;
            }

            using (new ScopedSpinLock(spinLock, new TimeSpan(10)))
            {
                obj.BitMask |= bits;
            }

            return ResultCodes.Success;
        }

        public virtual ChangeBitMask PotentialSystemChanges { get; }
    }
}
