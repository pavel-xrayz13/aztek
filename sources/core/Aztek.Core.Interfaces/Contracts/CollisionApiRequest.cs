﻿using Aztek.Core.Interface.Constants;
using System;
using System.Numerics;

namespace Aztek.Core.Interface.Contracts
{
    public class CollisionApiRequest
    {
        // Start position of the test
        public Vector3 StartPosition { get; set; }

        // End position of the test
        public Vector3 EndPosition { get; set; }

        // Type of test
        public CollisionType Type { get; set; }

        // Unique handle for this request
        public UIntPtr Handle { get; set; }

        // Name of object to ignore in collision
        public string Ignore { get; set; }

        // Flags
        public CollisionFlags Flags { get; set; }
    }
}
