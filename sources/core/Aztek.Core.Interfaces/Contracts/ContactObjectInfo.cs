﻿using System.Numerics;

namespace Aztek.Core.Interface.Contracts
{
    /// <summary>
    ///   Structure for providing object-to-object contact information.
    /// </summary>
    public class ContactObjectInfo
    {
        public Vector3 VelocityObjectA { get; set; }
        public Vector3 VelocityObjectB { get; set; }
        public Vector3 Normal { get; set; }
        public Vector3 Position { get; set; }
        public float Impact { get; set; }
        public bool Static { get; set; }
    }
}
