﻿using System.Numerics;

namespace Aztek.Core.Interface.Contracts
{
    /// <summary>
    ///   Structure for providing Intersection information.
    /// </summary>
    public class IntersectionObjectInfo
    {
        public string Name { get; set; }
        public Vector3 Position { get; set; }
        public Quaternion Orientation { get; set; }
        public Vector3 LinearVelocity { get; set; }
        public Vector3 AngularVelocity { get; set; }
        public Vector3 AABBMinimal { get; set; }
        public Vector3 AABBMaximal { get; set; }
    }
}
