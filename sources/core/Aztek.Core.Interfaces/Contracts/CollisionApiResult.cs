﻿using System.Numerics;

namespace Aztek.Core.Interface.Contracts
{
    public class CollisionApiResult
    {
        // Contact position
        public Vector3 Position { get; set; }

        // Normal vector (vector pernedicular to contact surface)
        public Vector3 Normal { get; set; }

        // Name of object hit (null is no collision detected)
        public string Hit { get; set; }

        // Penetration depth (along normal vector)
        public float Depth { get; set; }

        // Collision test has finished (0 = no, 1 = yes, >1 = delete)
        public uint Finished { get; set; }

        // A valid collision was detected
        public bool Valid { get; set; }
    }
}
