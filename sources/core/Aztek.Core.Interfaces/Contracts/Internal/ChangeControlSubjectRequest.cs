﻿using Aztek.Core.Interface.Constants;

namespace Aztek.Core.Interface.Contracts.Internal
{
    internal class ChangeControlSubjectRequest
    {
        public ChangeControlSubjectRequest(IChangeControlObserver observer, ChangeBitMask mask)
        {
            Observer = observer;
            BitMask = mask;
        }

        public IChangeControlObserver Observer { get; set; }
        public ChangeBitMask BitMask { get; set; }
    }
}
