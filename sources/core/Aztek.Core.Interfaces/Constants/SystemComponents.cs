﻿using Aztek.Core.BaseTypes.Attributes;

namespace Aztek.Core.Interface.Constants
{
    public enum SystemComponents
    {
        [Description("System")]
        System,

        [Description("Scene")]
        Scene,

        [Description("Object")]
        Object,

        [Description("Task")]
        Task
    }
}
