﻿namespace Aztek.Core.Interface.Constants
{
    public enum CollisionType
    {
        None,
        LineTest,
        Invalid
    }
}
