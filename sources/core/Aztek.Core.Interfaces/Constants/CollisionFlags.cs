﻿using System;

namespace Aztek.Core.Interface.Constants
{
    [Flags]
    public enum CollisionFlags
    {
        // No flags set (use defaults)
        None,

        // Only test against the ground
        Ground,

        // Exclude the ground from tests
        IgnoreGround
    }
}
