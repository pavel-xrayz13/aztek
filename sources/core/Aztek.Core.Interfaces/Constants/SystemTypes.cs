﻿using Aztek.Core.BaseTypes.Attributes;

namespace Aztek.Core.Interface.Constants
{
    /// <summary>
	///   An int enum, used to index the types of systems.
	///	  Custom systems do not have predefined types. Their types are made with
	///	  System::Types::MakeCustom(), so they're not listed here.
	/// </summary>
    public enum SystemTypes
    {
        [Description("NotUsed")]
        NotUsed = 0,

        [Description("GenericSystem")]
        Generic,

        [Description("GeometrySystem")]
        Geometry,

        [Description("GraphicsSystem")]
        Graphics,

        [Description("PhysicsCollisionSystem")]
        PhysicsCollision,

        [Description("AudioSystem")]
        Audio,

        [Description("InputSystem")]
        Input,

        [Description("AISystem")]
        AI,

        [Description("AnimationSystem")]
        Animation,

        [Description("ScriptingSystem")]
        Scripting,

        [Description("ParticlesSystem")]
        Particles,

        [Description("WaterSystem")]
        Water
    }
}
