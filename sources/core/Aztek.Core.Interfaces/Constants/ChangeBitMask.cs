﻿using System;

namespace Aztek.Core.Interface.Constants
{
    [Flags]
    public enum ChangeBitMask : uint
    {
        None = 0,
        GenericCreateObject = 1 << 0,
        GenericDeleteObject = 1 << 1,
        GenericExtendObject = 1 << 2,
        GenericUnextendObject = 1 << 3,
        GenericAll = GenericCreateObject | GenericDeleteObject | GenericExtendObject | GenericUnextendObject,

        PhysicsVelocity = AiVelocity, // Reusing this AI bit

        GeometryPosition = 1 << 8,
        GeometryScale = 1 << 10,
        GeometryOrientation = 1 << 9,
        GeometryAll = GeometryPosition | GeometryScale | GeometryOrientation,

        GraphicsIndexDecl = 1 << 12,
        GraphicsVertexDecl = 1 << 13,
        GraphicsIndexBuffer = 1 << 14,
        GraphicsVertexBuffer = 1 << 15,
        GraphicsAabb = 1 << 16,
        GraphicsAllMesh = GraphicsIndexDecl | GraphicsVertexDecl | GraphicsIndexBuffer | GraphicsVertexBuffer | GraphicsAabb,

        GraphicsParticlesDecl = 1 << 17,
        GraphicsParticles = 1 << 18,
        GraphicsAllParticles = GraphicsParticlesDecl | GraphicsParticles,

        GraphicsAnimation = 1 << 19,
        GraphicsUI = 1 << 20,

        GraphicsAll = GraphicsAllMesh | GraphicsAllParticles,

        PoiArea = 1 << 21,
        PoiContact = 1 << 22,
        PoiTarget = 1 << 23,
        PoiIntersection = 1 << 24,

        AiBehavior = 1 << 25,
        AiVelocity = 1 << 26, // Reusing this physics bit

        Link = 1 << 29,
        ParentLink = 1 << 30,
        Custom = (uint)1 << 31,

        All = uint.MaxValue
    }
}