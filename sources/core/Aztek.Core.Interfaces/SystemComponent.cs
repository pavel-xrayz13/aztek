﻿using Aztek.Core.BaseTypes;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface.Constants;
using System.Collections.Generic;

namespace Aztek.Core.Interface
{
    /// <summary>
    ///   <c>SystemComponent</c> is an abstract class designed to be a method for adding functionality to the
    ///   framework. By default the framework does not have functionality for things like graphics,
    ///   physics, etc.
    /// </summary>
    public abstract class SystemComponent
    {
        protected bool isInitilized = false;

        /// <summary cref="Initialize">
        ///   One time initialization function for the object.
        /// </summary>
        /// <param name="Properties">Property structure array to fill in.</param>
        /// <returns>An error code.</returns>
        public abstract ResultCodes Initialize([NotNull] IEnumerable<Property> properties);

        /// <summary cref="CreateScene">
        ///   Creates a system scene for containing system objects.
        /// </summary>
        /// <returns>The newly create system scene.</returns>
        [NotNull] public abstract SystemScene CreateScene();

        /// <summary cref="DestroyScene">
        ///   Destroys a system scene.
        /// </summary>
        /// <param name="systemScene">The scene to destroy. Any objects within are destroyed.</param>
        /// <returns>An error code.</returns>
        public abstract ResultCodes DestroyScene([NotNull] SystemScene systemScene);

        /// <summary cref="Properties">
        ///   Properties of this object.
        /// </summary>
        public abstract IEnumerable<Property> Properties
        {
            /// <summary>
            ///   Gets the properties of this object.
            /// </summary>
            /// <returns>The Property structure array to fill.</returns>
            [NotNull] get;

            /// <summary>
            ///   Sets the properties for this object.
            /// </summary>
            [NotNull] set;
        }

        /// <summary cref="Name">
        ///   Gets the name of the system. Only custom systems can return a custom name.
        ///   <para>
        ///	    Non-custom system must return a pre-defined name in Systems::Names that matches with the
        ///     type.
        ///	  </para>
        /// </summary>
        /// <returns>The name of the system.</returns>
        public abstract string Name { get; }

        /// <summary cref="SystemType">
        ///   Gets the system type for this system.
        /// </summary>
        /// <remarks>
        ///   This must function even w/o initialization.
        /// </remarks>
        /// <returns>The type of the system.</returns>
        public abstract SystemTypes SystemType { get; }
    }
}
