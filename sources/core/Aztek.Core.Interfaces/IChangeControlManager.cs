﻿using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface.Constants;

namespace Aztek.Core.Interface
{
    /// <summary>
    ///  The <c>IChangeControlObserver</c> interface supplies loosely coupling systems with dependency
    ///  It facilitates a lazy behaviour where by systems only need to react to a change
    ///	 and facilitates dependent callback threading.      
    /// </summary>
    /// <seealso cref="IChangeControlSubject"/>
    /// <seealso cref="IChangeControlObserver"/>
    /// <remarks>
    ///  This interface follows the pattern commonly know as the Observer
    ///	 pattern, the Publish/Subscribe pattern, or the Dependents pattern. 
    /// </remarks>
    /// <remarks>
    ///  The Observer pattern is documented in "Design Patterns," written by
    ///	 Erich Gamma et. al., published by Addison-Wesley in 1995.          
    /// </remarks>
    public interface IChangeControlManager
    {
        /// <summary cref="Register(IChangeControlSubject, ChangeBitMask, IChangeControlObserver, ChangeBitMask)">
        ///  Associates the provided IObserver with the given ISubject aspects of interest.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="subject">A pointer to the ISubject.</param>
        /// <param name="subjectBits">The aspects of interest that changed as defined by the supplied 
        ///				ISubject's published interest bits..</param>
        /// <param name="observer">A pointer to the IObserver.</param>
        /// <param name="observerBits">Bitmask of the interest bits. (default = System::Types::All)</param>
        /// <returns>
        ///   One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///     Error::OutOfMemory                                   
        ///			Not enough memory is available to resolve the change.
        ///</returns>
        ResultCodes Register([NotNull] IChangeControlSubject subject, ChangeBitMask subjectBits, 
            [NotNull] IChangeControlObserver observer, ChangeBitMask observerBits);

        /// <summary cref="Unregister(IChangeControlSubject, IChangeControlObserver)">
        ///   Disassociates the provided IObserver from the supplied ISubject.
        /// </summary>
        /// <param name="subject">A reference to the IChangeControlSubject.</param>
        /// <param name="observer">A reference to the IChangeControlObserver.</param>
        /// <returns>
        ///   One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///</returns>
        ResultCodes Unregister([NotNull] IChangeControlSubject subject, [NotNull] IChangeControlObserver observer);

        /// <summary>
        ///  Distributes the queued changes..
        /// </summary>
        /// <remarks>
        ///	 Intended to be called after all ISubject's have changed state to 
        ///	 deliver relevant queued change notifications to registered 
        ///	 IObservers via. IChangeControlObserver.Update().
        /// </remarks>
        /// <returns>One of the following Error codes:
        ///     Error::Success
        ///			No error.
        ///     Error::InvalidAddress 
        ///			pInObserver and/or pInSubject was NULL.
        ///     Error::OutOfMemory
        ///			Not enough memory is available to resolve the change.
        ///</returns>
        ResultCodes DistributeQueuedChanges(ChangeBitMask systemsBeNotified = ChangeBitMask.All, 
            ChangeBitMask changedToDistribute = ChangeBitMask.All);
    }
}
