﻿using Aztek.Core.BaseTypes;
using Aztek.Core.BaseTypes.Attributes;
using Aztek.Core.Interface.Constants;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;

namespace Aztek.Core.Interface
{
    /// <summary>
    ///   <c>SystemObject</c> is an abstract class designed to be an extension of the <c>UniversalObject</c>.
    ///   Systems can extend the <c>UniversalObject</c> by implementing this interface to give it new properties.
    ///   <para>
    ///     An example would be a physics system implementing this interface so that the <c>UniversalObject</c> would
    ///     now be able to interact with the physics system.
    ///   </para>
    /// </summary>
    public abstract class SystemObject : ChangeControlSubject, IChangeControlObserver
    {
        /// <summary>
        ///   Constructor.
        /// </summary>
        /// <remarks>
        ///   Inlined for performance.
        /// </remarks>
        /// <param name="systemScene">The scene this object belongs to.</param>
        /// <param name="name">Name of this system object.</param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        protected SystemObject([NotNull] SystemScene systemScene, [NotNull] string name)
        {
            Contract.Requires(systemScene != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            SystemScene = systemScene;
            Name = name;
        }

        public virtual ResultCodes ChangeOccurred([NotNull] IChangeControlSubject subject, ChangeBitMask changeBitMask)
        {
            return ResultCodes.Success;
        }

        /// <summary cref="Initialize">
        ///   One time initialization function for the object.
        /// </summary>
        /// <param name="Properties">Property structure array to fill in.</param>
        /// <returns>An error code.</returns>
        public abstract ResultCodes Initialize([NotNull] IEnumerable<Property> properties);

        /// <summary cref="Properties">
        ///   Properties of this object.
        /// </summary>
        public abstract IEnumerable<Property> Properties
        {
            /// <summary>
            ///   Gets the properties of this object.
            /// </summary>
            /// <returns>The Property structure array to fill.</returns>
            get;

            /// <summary>
            ///   Sets the properties for this object.
            /// </summary>
            set;
        }

        /// <summary cref="GetChangeBitMask">
        ///   Returns a bit mask of System Changes that this system wants to receive changes for.  Used
        ///    to inform the change control manager if this system's object should be informed of the
        ///    change.
        /// </summary>
        /// <returns>A System::Changes::BitMask.</returns>
        public abstract ChangeBitMask GetChangeBitMask { get; }

        /// <summary>
        ///   Gets the system type for this system object.
        /// </summary>
        /// <remarks>
        ///   This is a shortcut to getting the system type w/o having to go the system first.
        /// </remarks>
        /// <returns>The type of the system.</returns>
        public abstract SystemTypes SystemType { get; }

        /// <summary cref="SystemScene">
        ///   Gets the system scene this object belongs to.
        /// </summary>
        /// <returns>A reference to the system.</returns>
        public SystemScene SystemScene
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)] get;
        }

        /// <summary cref="Name">
        ///   Gets the name of the object.
        /// </summary>
        /// <returns>The name of the object.</returns>
        public string Name
        {
            [MethodImpl(MethodImplOptions.AggressiveInlining)] get;
        }
    }
}
