﻿using Aztek.Core.Interface.Contracts.Internal;
using Aztek.Core.Interface.Constants;

namespace Aztek.Core.Interface.Extensions.Internal
{
    internal static class ChangeControlSubjectRequestExtensions
    {
        public static ChangeBitMask GetBitsToPost(this ChangeControlSubjectRequest request, ChangeBitMask changedBits)
        {
            var result = request.BitMask & changedBits;
            return result;
        }
    }
}
