﻿using Aztek.Core.DI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace microDI.UnitTests.Tests
{
    [TestClass]
    public class AutoWireTests
    {
        [TestMethod]
        public void Register_SimpleInterface_AutoWireWithRegistered_Test()
        {
            var container = new Container();

            Assert.DoesNotThrow(() => container.RegisterAs<IFirstInterface, SimpleClassImplementsTwoInterfaces>(
                new TransientLifeCyclePolicy()).AutoWire<ISecondInterface>());
        }

        [TestMethod]
        public void RegisterAndResolve_WithAutoWire_TwoInterfacesWithImplementor_AutoWireWithRegistered()
        {
            var container = new Container();

            Assert.DoesNotThrow(() => container.RegisterAs<IFirstInterface, SimpleClassImplementsTwoInterfaces>(
                new TransientLifeCyclePolicy()).AutoWire<ISecondInterface>());

            Assert.DoesNotThrow(() => container.Resolve<IFirstInterface>());

            Assert.DoesNotThrow(() => container.Resolve<ISecondInterface>());
        }
    }
}
